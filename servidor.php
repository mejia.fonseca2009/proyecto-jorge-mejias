<?php
     $moneda=array("moneda"=> array(
        array(
            "code" => "USD",
            "name" => "Dólar Estadounidense"
        ),array(
            "code" => "EUR",
            "name" => "Euro"
        ),array(
            "code" => "BSS",
            "name" => "Bolívar Soberano"
        ),array(
            "code" => "ARS",
            "name" => "Peso Argentino"
        ),array(
            "code" => "COP",
            "name" => "Peso Colombiano"
        ),
    ));
echo json_encode($moneda);
?>



 <?php
     $tasa=array("tasa"=> array(
        array(
        'from'=> 'USD',
       'to'=> 'EUR',
       'rate'=> '0.88'
    ),array(
       'from'=> 'EUR',
       'to'=> 'BSS',
       'rate'=> '3726.04'
    ),array(
       'from'=> 'EUR',
       'to'=> 'ARS',
       'rate'=> '46.34'
    ),array(
       'from'=> 'USD',
       'to'=> 'COP',
       'rate'=> '3083.05'
    ),
));
echo json_encode($tasa);
?> 



<?php
     $transacion=array("transacion"=> array(
        array(
        'code'=> 'C5277',
       'amount'=> '97.5',
       'currency'=> 'USD'
    ),array(
       'code'=> 'C5277',
       'amount'=> '3990.77',
       'currency'=> 'AUD'
    ),array(
       'code'=> 'W0049',
       'amount'=> '350',
       'currency'=> 'USD'
    ),array(
       'code'=> 'R4385',
       'amount'=> '709101.50',
       'currency'=> 'COP'
    ),array(
       'code'=> 'D4630',
       'amount'=> '12279.30',
       'currency'=> 'AUD'
    ),array(
       'code'=> 'D4630',
       'amount'=> '987426',
       'currency'=> 'BSS'
    ),
));
echo json_encode($transacion);
?>



<?php
     $productos=array("productos"=> array(
        array(
       'code'=> 'C5277',
       'name'=> 'TV'
    ),array(
       'code'=> 'W0049',
       'name'=> 'PC'
    ),array(
       'code'=> 'D4630',
       'name'=> 'LAPTOP'
    ),array(
       'code'=> 'R4385',
       'currency'=> 'PHONE'
    ),
));
echo json_encode($productos);
?>  